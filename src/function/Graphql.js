import {
    ApolloClient,
    InMemoryCache,
    gql,
    createHttpLink,
} from "@apollo/client";
import { setContext } from "@apollo/client/link/context";
import moment from "moment";

const server = createHttpLink({
    uri: "http://localhost:4000/graphql",
});
const authLink = setContext((_, { headers }) => {
    return {
        headers: {
            ...headers,
            Action: "Login",
            'X-Date': `${moment().format("MMMM Do YYYY")}`
        },
    };
});
const authLinkk = setContext((_, { headers }) => {
    const token = JSON.parse(localStorage.getItem('wizcard')).token.token
    return {
        headers: {
            ...headers,
            Action: "User",
            authorization: token ? `Bearer ${token}` : ''
        },
    };
});
const client = new ApolloClient({
    link: authLink.concat(server),
    cache: new InMemoryCache(),
});
const clientt = new ApolloClient({
    link: authLinkk.concat(server),
    cache: new InMemoryCache(),
});

export const checkLicense = async (key) => {
    try {
        let check = await client.query({
            query: gql`
                {
                    license(key: "${key}") {
                        key
                        registered
                        expired
                        type
                        model
                        used
                    }
                }
            `,
        })
        return check.data.license;
    } catch (error) {
        return false

    }
};

export const checkToken = async (key) => {
    try {
        let check = await client.query({
            query: gql`
                {
                    token(key: "${key}") {
                        token
                        expired
                    }
                }
            `,
        });
        if (check.data.token) {
            if (parseInt(check.data.token.expired) < Date.now()) {
                await deleteToken(key)
                let tok = await createToken(key)
                return tok
            }
        }
        return check.data.token;
    } catch (error) {
        return false
    }
};

export const createToken = async (key) => {
    try {
        let start = await client.mutate({
            mutation: gql`
                mutation{
                    createToken(key: "${key}") {
                        token
                        expired
                    }
                }
            `,
        });
        return start.data.createToken;
    } catch (error) {
        return false
    }
};

export const deleteToken = async (key) => {
    try {
        return await client.mutate({
            mutation: gql`
                mutation {
                    deleteToken(key: "${key}") {
                        token
                    }
                }
            `,
        });
    } catch (error) {
        return false
    }
};

export const createProduct = async (key, email, ip, domain) => {
    let check = await getProduct(key)
    if (check) {
        return false
    } else {
        try {
            let query = await clientt.mutate({
                mutation: gql`
                    mutation {
                        createProduct(key: "${key}", email: "${email}", ip: "${ip}", domain: "${domain}") {
                            domain
                            ip
                            email
                            status
                        }
                    }
                `
            })
            let data = query.data.createProduct
            return data
        } catch (error) {
            if (error.message == 'Unauthorized') {
                localStorage.removeItem('wizcard')
                window.location.replace('../../')
                return
            }
            return false
        }
    }
}

export const getProduct = async (key) => {
    try {
        let query = await clientt.query({
            query: gql`
                query {
                    product(key: "${key}") {
                        domain
                        ip
                        email
                        status
                    }
                }
            `
        })
        return query.data.product
    } catch (error) {
        if (error.message == 'Unauthorized') {
            localStorage.removeItem('wizcard')
            window.location.replace('../../')
            return
        }
        return false
    }
}

export const delProduct = async (key) => {
    try {
        let query = await clientt.mutate({
            mutation: gql`
                mutation {
                    deleteProduct(key: "${key}") {
                        domain
                    }
                }
            `
        })
        return query.data
    } catch (error) {
        if (error.message == 'Unauthorized') {
            localStorage.removeItem('wizcard')
            window.location.replace('../../')
            return
        }
        return false
    }
}

export const checkAdmin = async () => {
    try {
        let check = await clientt.query({
            query: gql`
                query {
                    admin {
                        key
                    }
                }
            `
        })
        return check
    } catch (error) {
        return false
    }
}