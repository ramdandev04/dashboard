import React from "react";
import { Tooltip } from "bootstrap";
import Swal from "sweetalert2";
import { delProduct, getProduct } from "../function/Graphql";

class Tablesetup extends React.Component {
    state = {
        product: {
            ip: "_",
            domain: "_",
            email: "_",
            status: "_",
        },
    };
    componentDidMount = async () => {
        let local = JSON.parse(localStorage.getItem("wizcard"));
        let getSource = await getProduct(local.key);
        let state = { ...this.state };
        state.product = getSource;
        this.setState(state);
        var tooltipTriggerList = [].slice.call(
            document.querySelectorAll('[data-bs-toggle="tooltip"]')
        );
        var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new Tooltip(tooltipTriggerEl);
        });
    };
    pauseServer = () => {
        let local = localStorage.getItem("server");
        local = JSON.parse(local);
        let state = { ...this.state };
        Swal.fire({
            title: "Action to server",
            text: "Are you sure want to pause the server?",
            showCancelButton: true,
            icon: "question",
        }).then((res) => {
            if (res.isConfirmed) {
                local.status = "stopped";
                localStorage.setItem("server", JSON.stringify(local));
                state.data.status = "stopped";
                this.setState(state);
            }
        });
    };
    startServer = () => {
        let local = localStorage.getItem("server");
        local = JSON.parse(local);
        let state = { ...this.state };
        Swal.fire({
            title: "Action to server",
            text: "Start this server?",
            showCancelButton: true,
            icon: "question",
        }).then((res) => {
            if (res.isConfirmed) {
                local.status = "running";
                localStorage.setItem("server", JSON.stringify(local));
                state.data.status = "running";
                this.setState(state);
            }
        });
    };
    terminateServer = async () => {
        let res = await Swal.fire({
            title: "Action to server",
            text: "Terminate this app?",
            showCancelButton: true,
            icon: "question",
        });
        if (res.isConfirmed) {
            let local = JSON.parse(localStorage.getItem("wizcard"));
            await delProduct(local.key);
            setTimeout(() => {
                window.location.reload();
            }, 2000);
        }
    };
    render() {
        return (
            <table className="table table-dark mt-5 table-bordered text-center">
                <thead>
                    <tr>
                        <th scope="col">Ip Address</th>
                        <th scope="col">Domain name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{this.state.product.ip}</td>
                        <td>{this.state.product.domain}</td>
                        <td>{this.state.product.email}</td>
                        <td>
                            <span
                                data-bs-toggle="tooltip"
                                data-bs-placment="top"
                                title="this is the status of your server"
                                className={`badge ${
                                    this.state.product.status === "running"
                                        ? "bg-success"
                                        : this.state.product.status ===
                                          "stopped"
                                        ? "bg-danger"
                                        : "bg-secondary"
                                }`}
                            >
                                {this.state.product.status === "running"
                                    ? "running"
                                    : this.state.product.status === "stopped"
                                    ? "stopped"
                                    : "unknown"}
                            </span>
                        </td>
                        <td>
                            <span
                                onClick={
                                    this.state.product.status === "running"
                                        ? this.pauseServer
                                        : this.state.product.status ===
                                          "stopped"
                                        ? this.startServer
                                        : (e) => e.preventDefault()
                                }
                                data-bs-toggle="tooltip"
                                data-bs-placment="top"
                                title="Action to stop / start the server"
                                className={`badge ${
                                    this.state.product.status === "running"
                                        ? "bg-warning"
                                        : this.state.product.status ===
                                          "stopped"
                                        ? "bg-success"
                                        : "bg-secondary"
                                }`}
                            >
                                {this.state.product.status === "running"
                                    ? "pause"
                                    : this.state.product.status === "stopped"
                                    ? "start"
                                    : "unknown"}
                            </span>
                            <span style={{ marginLeft: "5px" }}></span>
                            <span
                                onClick={this.terminateServer}
                                data-bs-toggle="tooltip"
                                data-bs-placment="top"
                                title="Terminating mean you delete the app installed on server"
                                className={`badge bg-danger`}
                            >
                                terminate
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
        );
    }
}

export default Tablesetup;
