import React from "react";
import { Bar } from "react-chartjs-2";

class Dashchart extends React.Component {
    state = {
        user: {
            ok: false,
            data: {
                card: "_",
                login: "_",
                visit: "_",
                bot: "_",
            },
        },
        dataSet: {
            labels: ["ads", "login", "visit", "bot"],
            datasets: [
                {
                    data: [],
                    backgroundColor: [
                        "rgba(248, 245, 47, 0.699)",
                        "#27ee20a1",
                        "#20adeea1",
                        "#ee2020a1",
                    ],
                    borderColor: ["#fffb00", "#12f70a", "#20cfee", "#f70808"],
                    borderWidth: "2",
                },
            ],
        },
    };
    updateChart = () => {
        let state = { ...this.state };
        let data = this.state.user.data;
        state.dataSet.datasets[0].data = [
            data.card,
            data.login,
            data.visit,
            data.bot,
        ];
        this.setState(state);
    };
    componentDidMount = () => {
        var local = localStorage.getItem("data");
        local = JSON.parse(local);
        let state = { ...this.state };
        if (local !== null) {
            if (local.ok && !this.state.user.ok) {
                state.user.ok = true;
                this.setState(state);
            }
        }
        if (this.state.user.ok) {
            state.user.data = local.user.data;
            this.setState(state);
        }
        this.updateChart();
    };
    render() {
        return (
            <div className="col">
                <div className="bg-dark mt-3">
                    <Bar
                        data={this.state.dataSet}
                        height={120}
                        options={{ legend: { display: false } }}
                    />
                </div>
            </div>
        );
    }
}

export default Dashchart;
