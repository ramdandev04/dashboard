import React from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { checkAdmin, deleteToken } from "../function/Graphql";

const Mylink = (props) => {
    const handleClick = (e) => {
        if (props.href === "/signout") {
            e.preventDefault();
            let Alert = Swal.mixin({
                customClass: {
                    confirmButton: "btn btn-danger",
                    cancelButton: "btn btn-primary",
                },
            });
            Alert.fire({
                showCancelButton: true,
                title: "Log Out",
                text: "Are you sure?",
                icon: "question",
                cancelButtonColor: "dodgerblue",
                confirmButtonText: "Log Out",
                confirmButtonColor: "red",
            }).then(async (result) => {
                if (result.isConfirmed) {
                    let local = localStorage.getItem("wizcard");
                    local = JSON.parse(local);
                    await deleteToken(local.key);
                    localStorage.removeItem("wizcard");
                    Swal.fire({
                        text: "Logged Out",
                        toast: true,
                        timer: 1500,
                        showConfirmButton: false,
                        position: "top-end",
                        icon: "success",
                    });
                }
            });
            return;
        }
        props.setPath(props.href);
    };

    return (
        <Link onClick={handleClick} to={props.href}>
            {props.children}
        </Link>
    );
};

class Navlink extends React.Component {
    state = {
        path: "/",
        admin: false,
    };

    setPath = (value) => {
        let state = { ...this.state };
        state.path = value;
        this.setState(state);
    };

    componentDidMount = async () => {
        let path = window.location.pathname;
        let check = await checkAdmin();
        if (check) {
            let state = { ...this.state };
            state.admin = true;
            this.setState(state);
        }
        this.setPath(path);
    };

    render() {
        return (
            <>
                <div
                    className={`navlink ${
                        this.state.path === "/" ? "nactive" : ""
                    }`}
                >
                    <Mylink setPath={this.setPath} href="/">
                        <i
                            className="fas fa-tv"
                            style={{ color: "dodgerblue" }}
                        ></i>
                        <span>Dashboard</span>
                    </Mylink>
                </div>
                <div
                    className={`navlink ${
                        this.state.path === "/setup" ? "nactive" : ""
                    }`}
                >
                    <Mylink setPath={this.setPath} href="/setup">
                        <i
                            className="fas fa-cogs"
                            style={{ color: "#FF0000" }}
                        ></i>
                        <span>Setup</span>
                    </Mylink>
                </div>
                <div
                    className={`navlink ${
                        this.state.path === "/logs" ? "nactive" : ""
                    }`}
                >
                    <Mylink setPath={this.setPath} href="/logs">
                        <i
                            className="fas fa-history"
                            style={{ color: "#DF00FF" }}
                        ></i>
                        <span>Logs</span>
                    </Mylink>
                </div>
                <div
                    className={`navlink ${
                        this.state.path === "/license" ? "nactive" : ""
                    }`}
                >
                    <Mylink setPath={this.setPath} href="/license">
                        <i
                            className="fab fa-centos"
                            style={{ color: "#DBBC15" }}
                        ></i>
                        <span>License</span>
                    </Mylink>
                </div>
                {this.state.admin ? (
                    <div
                        className={`navlink ${
                            this.state.path === "/admin" ? "nactive" : ""
                        }`}
                    >
                        <Mylink setPath={this.setPath} href="/admin">
                            <i
                                className="fab fa-xing"
                                style={{ color: "purple" }}
                            ></i>
                            <span>Admin</span>
                        </Mylink>
                    </div>
                ) : (
                    ""
                )}
                <div
                    className={`navlink ${
                        this.state.path === "/signout" ? "nactive" : ""
                    }`}
                >
                    <Mylink setPath={this.setPath} href="/signout">
                        <i className="fas fa-sign-out-alt"></i>
                        <span>Log Out</span>
                    </Mylink>
                </div>
            </>
        );
    }
}

export default Navlink;
