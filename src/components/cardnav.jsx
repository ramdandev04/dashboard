import React from "react";

class Cardnav extends React.Component {
    state = {
        user: {
            card: "_",
            login: "_",
            visit: "_",
            bot: "_",
        },
    };
    componentDidMount = () => {
        let local = localStorage.getItem("wizcard");
    };
    render() {
        return (
            <div className="row">
                <div className="col">
                    <div className="card">
                        <div className="card-body">
                            <div className="d-flex justify-content-between">
                                <div>
                                    <h5 className="card-title">Ads earnings</h5>
                                    <p className="card-desc">
                                        {this.state.user.card}
                                    </p>
                                </div>
                                <div className="card-icon">
                                    <i className="fas fa-credit-card"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card">
                        <div className="card-body d-flex justify-content-between">
                            <div>
                                <h5 className="card-title">Account</h5>
                                <p>{this.state.user.login}</p>
                            </div>
                            <div className="card-icon">
                                <i className="fas fa-user-circle"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card">
                        <div className="card-body d-flex justify-content-between">
                            <div>
                                <h5 className="card-title">Visitor</h5>
                                <p>{this.state.user.visit}</p>
                            </div>
                            <div className="card-icon">
                                <i className="fas fa-paper-plane"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <div className="card">
                        <div className="card-body d-flex justify-content-between">
                            <div>
                                <h5 className="card-title">Bot</h5>
                                <p>{this.state.user.bot}</p>
                            </div>
                            <div className="card-icon">
                                <i className="fas fa-robot"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Cardnav;
