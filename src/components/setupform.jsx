import React from "react";
import Swal from "sweetalert2";
import { createProduct } from "../function/Graphql";

class Setupform extends React.Component {
    state = {
        input: {
            a: "",
            b: "",
            c: "",
        },
    };
    componentDidMount = () => {
        let local = localStorage.getItem("reload");
        if (local) {
            localStorage.removeItem("reload");
            window.location.replace("../../");
        }
    };
    changeA = (e) => {
        let state = { ...this.state };
        // return console.log(e);
        state.input.a = e.target.value;
        this.setState(state);
    };
    changeB = (e) => {
        let state = { ...this.state };
        // return console.log(e);
        state.input.b = e.target.value;
        this.setState(state);
    };
    changeC = (e) => {
        let state = { ...this.state };
        // return console.log(e);
        state.input.c = e.target.value;
        this.setState(state);
    };
    handleSubmit = async (e) => {
        e.preventDefault();
        let local = JSON.parse(localStorage.getItem("wizcard"));
        let data = {
            ip: this.state.input.c,
            domain: this.state.input.b,
            email: this.state.input.a,
        };
        let cret = await createProduct(
            local.key,
            data.email,
            data.ip,
            data.domain
        );
        if (!cret) {
            return Swal.fire({
                toast: true,
                title: "Something went wrong",
                icon: "error",
                showConfirmButton: false,
                timer: 1200,
                position: "top-end",
            });
        }
        Swal.fire({
            text: "Success, server has been deployed",
            icon: "success",
            toast: true,
            showConfirmButton: false,
            position: "top-end",
        });
        localStorage.setItem("reload", true);
        setTimeout(() => {
            window.location.reload();
        }, 1500);
    };
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="mb-3 text-center">
                    <label htmlFor="email" className="form-label">
                        Email address
                    </label>
                    <input
                        type="email"
                        className="form-control"
                        id="email"
                        onChange={this.changeA}
                        value={this.state.input.a}
                        required
                    />
                </div>
                <div className="mb-3 text-center">
                    <label htmlFor="domain" className="form-label">
                        Domain name
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="domain"
                        onChange={this.changeB}
                        value={this.state.input.b}
                        required
                    />
                </div>
                <div className="mb-3 text-center">
                    <label htmlFor="server" className="form-label">
                        Server ip
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="server"
                        onChange={this.changeC}
                        value={this.state.input.c}
                        required
                    />
                </div>
                <div className="text-center">
                    <button type="submit" className="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        );
    }
}

export default Setupform;
