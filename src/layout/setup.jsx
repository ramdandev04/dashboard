import React from "react";
import NProgress from "nprogress";
import Tablesetup from "../components/setupok";
import Setupcreate from "../components/setupcreate";
import { getProduct } from "../function/Graphql";

class Setup extends React.Component {
    state = {
        ready: false,
        product: {},
    };
    componentWillUnmount() {
        NProgress.start();
    }
    componentDidMount = async () => {
        let local = localStorage.getItem("wizcard");
        local = JSON.parse(local);
        if (local) {
            let prod = await getProduct(local.key);
            if (prod) {
                let state = { ...this.state };
                if (prod) {
                    state.product = prod;
                    state.ready = true;
                    this.setState(state);
                }
                if (local.used) {
                    state.user.ok = true;
                    this.setState(state);
                }
            }
            NProgress.done();
        }
    };
    render() {
        return (
            <div className="setup container">
                <h1>Setup</h1>
                {this.state.ready ? (
                    <Tablesetup state={this.state} />
                ) : (
                    <Setupcreate />
                )}
            </div>
        );
    }
}

export default Setup;
