import React from "react";
import NProgress from "nprogress";
import Cardnav from "../components/cardnav";
import Dashchart from "../components/dashchart";
import { checkLicense } from "../function/Graphql";

class Dashboard extends React.Component {
    componentWillUnmount() {
        NProgress.start();
    }
    componentDidMount = async () => {
        setInterval(async () => {
            let local = localStorage.getItem("wizcard");
            if (local !== null) {
                local = JSON.parse(local);
                let chk = await checkLicense(local.key);
                if (!chk) {
                    localStorage.removeItem("wizcard");
                }
            } else {
                window.location.reload();
            }
        }, 60000);
        NProgress.done();
    };
    render() {
        return (
            <div className="dashboard container">
                <h1>Dashboard</h1>
                <div className="mt-3">
                    <Cardnav />
                    <Dashchart />
                </div>
            </div>
        );
    }
}

export default Dashboard;
